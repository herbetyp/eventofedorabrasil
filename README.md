> # **Introdução ao Python**

## **Palestra do 1 evento da [Comunidade Fedora Brasil](https://fedorabr.org/)**


![Banner](img/banner.jpg)


- [ ] Tipos de dados: __string__, __int__, __float__, __bool__, __none__
- [ ] Operadores atitméticos: __/__, __*__, __+__, __-__, __**__, __//__, __%__, 
- [ ] Condicionais: __if__, __elif__, __else__
- [ ] Operadores lógicos: __and__, __or__, __not__
- [ ] Coleções de dados: __list__, __tuple__, __dict__, __set__
- [ ] Loops: __for__, __while__
- [ ] Funções em Python
- [ ] Classes
- [ ] Atributos
- [ ] Métodos
- [ ] Instância

---